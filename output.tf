output "subnet-gce" {
	value = google_compute_subnetwork.testing-gce.id
}

output "subnet-gke" {
	value = google_compute_subnetwork.testing-gke.id
}
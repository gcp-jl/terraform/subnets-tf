provider "google" {
    region  = var.region
    zone    = var.zone
}

data "terraform_remote_state" "project" {
	backend = "gcs" 

	config = {
    bucket = "st-terraform-test"
    prefix = "testing/terraform.tfstate"
	}
}

resource "google_compute_subnetwork" "testing-gce" {
    name = join("-",[var.name,"gce"])
    ip_cidr_range = var.ip_range[0]
    project = data.terraform_remote_state.project.outputs.project 

    description = var.description

    network = data.terraform_remote_state.project.outputs.network

    private_ip_google_access = false ### This allow or denies access from an external ip
}

resource "google_compute_subnetwork" "testing-gke" {
    name = join("-",[var.name,"gke"])
    ip_cidr_range = var.ip_range[1]
    project = data.terraform_remote_state.project.outputs.project

    network = data.terraform_remote_state.project.outputs.network

    secondary_ip_range {
            range_name = "pods"
            ip_cidr_range = var.ip_range[2]
        }
        
    secondary_ip_range {
            range_name = "services"
            ip_cidr_range = var.ip_range[3]
        }
    

    private_ip_google_access = false ### This allow or denies access from an external ip
}

variable "project" {
    description = "Project's Id"
    type = string
    default = "testing-160070"
}   
variable "region" {
    description = "Region where the object will be"
    type = string
    default = "us-central1"
}
variable "zone" {
    description = "Zone Selected as default"
    type = string
    default = "us-central1-a"
}   

variable "name" {
    description = "Subnet's Name"
    type = string
    default = "subnet-prueba"
}   
variable "ip_range" {
    description = "Range to subnet"
    type = list
}
variable "description" {
    type = string
    default = ""
}   

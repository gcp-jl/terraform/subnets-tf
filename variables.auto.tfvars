name = "testing"
ip_range = [
    "10.20.0.0/29",     ### ip_range GCE
    "10.20.10.0/28",    ### ip_range GKE nodes
    "10.30.0.0/21",     ### ip_range pods
    "10.30.8.0/24"      ### ip_range services
    ]
description = "This Subnet was created to use in VMs"